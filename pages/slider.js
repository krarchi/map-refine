import React from 'react';
import PropTypes from 'prop-types';
import { withStyles, makeStyles } from '@material-ui/core/styles';
import Slider from '@material-ui/core/Slider';
import Typography from '@material-ui/core/Typography';
import Tooltip from '@material-ui/core/Tooltip';

const useStyles = makeStyles((theme) => ({
  root: {
    width: '100%',
  },
  margin: {
    height: theme.spacing(3),
  },
}));





const PrettoSlider = withStyles({
  root: {
    color: 'orange',
    height: 8,
  },
  thumb: {
    height: 24,
    width: 24,
    backgroundColor: '#fff',
    border: '2px solid currentColor',
    marginTop: -8,
    marginLeft: -12,
    '&:focus, &:hover, &$active': {
      boxShadow: 'inherit',
    },
  },
  active: {},
  valueLabel: {
    left: 'calc(-50% + 4px)',
  },
  track: {
    height: 8,
    borderRadius: 4,
  },
  rail: {
    height: 8,
    borderRadius: 4,
  },
})(Slider);


function valuetext(value) {
  return `${value}°C`;
}
export default function CustomizedSlider(props) {
  const classes = useStyles();
  const [value, setValue] = React.useState([props.min, props.max]);
  const handleChange = (event, newValue) => {
    setValue(newValue);
    props.change(newValue)
  };
  return (
    <div className={classes.root} style={{marginLeft:'10px'}}>

      
      
      <Typography gutterBottom id = 'range-slider'></Typography>
      <PrettoSlider 
       value={value}
       onChange={handleChange}
       valueLabelDisplay="auto"
       aria-labelledby="range-slider"
       getAriaValueText={valuetext}
      />
      
      
    </div>
  );
}
