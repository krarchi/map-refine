import Head from 'next/head'
import Image from 'next/image'
import styles from '../styles/Home.module.css'
import SimpleMap from './refine'

export default function Home() {
  return (
   <div>
   <SimpleMap/>
   </div>
  )
}
