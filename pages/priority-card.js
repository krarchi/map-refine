import Head from 'next/head'
import Image from 'next/image'
import styles from '../styles/Home.module.css'
import StarBorderIcon from '@material-ui/icons/StarBorder';
import ImportExportIcon from '@material-ui/icons/ImportExport';

const selectedStar = {
    color: 'orange'
}

export default function PriorityCard(props) {
  return (
   <div class='Rcard'>
      <div class='row'>
          <div class='col-2'>
              <div className='proxImage'>
              <img src='/park.png' height= '40'></img>
              </div>
          </div>
        <div class='col'>
            <div style={{marginLeft: '30px'}}>
            <div class='row'>
               Proximity to {props.name}
            </div>
            <div class='row'>
                <div class='rating'>
                    <StarBorderIcon style={props.score >= 1 ? selectedStar : {} } onClick={() => {props.change(props.id,1)}}/>
                    <StarBorderIcon style={props.score >= 2 ? selectedStar : {} } onClick={() => {props.change(props.id,2)}}/>
                    <StarBorderIcon style={props.score >= 3 ? selectedStar : {} } onClick={() => {props.change(props.id,3)}}/>
                    <StarBorderIcon style={props.score >= 4 ? selectedStar : {} } onClick={() => {props.change(props.id,4)}}/>
                    <StarBorderIcon style={props.score >= 5 ? selectedStar : {} } onClick={() => {props.change(props.id,5)}}/>
                </div>
            </div>
            </div>
        </div>
        <div class='col-2'>
                <ImportExportIcon style={{height:'100%'}}/>
        </div>
      </div>
   </div>
  )
}