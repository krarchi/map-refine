import Head from 'next/head'
import Image from 'next/image'
import styles from '../styles/Home.module.css'

export default function Marker() {
  return (
   <div>
       <img src = '/pointerIcon.png' width='50'></img>
   </div>
  )
}