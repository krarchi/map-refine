import Head from 'next/head'
import Image from 'next/image'
import styles from '../styles/Home.module.css'



export default function Structure(props) {
  return (
   <div className={props.class}>
       <div class='row struct-img'><img src = {props.src} width='50'></img></div>
       <div class='row struct-text'><span>{props.name}</span></div>
   </div>
  )
}