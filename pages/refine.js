import GoogleMapReact from 'google-map-react';
import { Component } from 'react';
import Marker from './marker';
import dynamic from 'next/dynamic';
import ArrowForwardIcon from '@material-ui/icons/ArrowForward';
import RangeSlider from './slider'
import Structure from './structure-card';
import PriorityCard from './priority-card';

const Multiselect = dynamic(
    () => import('multiselect-react-dropdown').then(module => module.Multiselect),
    {
        ssr: false
    }
)
 
export default class SimpleMap extends Component {
  static defaultProps = {
    center: {
      lat: 54.55,
      lng: 30.33
    },
    zoom: 9,
    data: [
        {
            lat: 54.90,
            lng: 30.33,
            municipality: 1,
            nearby: [1,2],
            structureList: ['flat','apartment'],
            minPrice: 30,
            maxPrice: 100
          },
          {
            lat: 54.95,
            lng: 30.33,
            municipality: 1,
            nearby: [2,3],
            structureList: ['flat','apartment'],
            minPrice: 20,
            maxPrice: 100
          },{
            lat: 54.55,
            lng: 30.33,
            municipality: 2,
            nearby: [3,4],
            structureList: ['villa','flat','apartment'],
            minPrice: 40,
            maxPrice: 80
          },{
            lat: 54.65,
            lng: 30.33,
            municipality: 2,
            nearby: [5,4],
            structureList: ['villa','flat','apartment'],
            minPrice: 40,
            maxPrice: 100
          },{
            lat: 54.75,
            lng: 30.33,
            municipality: 3,
            nearby: [2,4,1],
            structureList: ['flat','detached','villa'],
            minPrice: 30,
            maxPrice: 100
          },{
            lat: 54.70,
            lng: 30.33,
            municipality: 3,
            nearby: [1,5,3],
            structureList: ['flat','villa','apartment'],
            minPrice: 50,
            maxPrice: 100
          },{
            lat: 54.70,
            lng: 30.30,
            municipality: 4,
            nearby: [1,4],
            structureList: ['flat','villa'],
            minPrice: 60,
            maxPrice: 100
          },
          {
            lat: 54.70,
            lng: 30.38,
            municipality: 4,
            nearby: [1,2,5],
            structureList: ['flat','detached','villa'],
            minPrice: 40,
            maxPrice: 100
          },

          {
            lat: 54.70,
            lng: 30.23,
            municipality: 5,
            nearby: [3,2,5],
            structureList: ['villa','apartment'],
            minPrice: 30,
            maxPrice: 80
          },
          {
            lat: 54.70,
            lng: 30.13,
            municipality: 5,
            nearby: [4,2,3],
            structureList: ['flat','villa'],
            minPrice: 70,
            maxPrice: 100
          },{
            lat: 54.70,
            lng: 30.17,
            municipality: 1,
            nearby: [5,2,4],
            structureList: ['villa','apartment','detached'],
            minPrice: 25,
            maxPrice: 50
          },
          {
            lat: 54.77,
            lng: 30.13,
            municipality: 2,
            nearby: [1,5,2],
            structureList: ['apartment','flat'],
            minPrice: 10,
            maxPrice: 80
          },
    ]
  };
  constructor (props) {
    super(props)
    this.state = {
        markerList  : this.props.data,
        municipality : [{name:'ABC',id:1},{name:'XYZ',id:2},{name:'NZC',id:3},{name:'UZU',id:4},{name:'MYMW',id:5}],
        priorities: [{name:'priority1',id:1,score: 0},{name:'priority2',id:2,score: 0},{name:'priority3',id:3,score: 0},{name:'priority4',id:4,score: 0},{name:'priority5',id:5,score: 0}],
        apartmentFlag: false,
        flatFlag: false,
        villaFlag: false,
        detachedFlag: false,
        minPrice: 0,
        maxPrice: 100,
        priorityList:[],
        structureList:[],
        municipalityList: []
    }

    this.style = {
      searchBox: {
        border: "none",
        "border-bottom": "4px solid #FBB917",
        "border-radius": "0px"
      },
      chips: { // To change css chips(Selected options)
        background: '#20B2AA'
        }
    };
   this.onSelect =  this.onSelect.bind(this);
   this.handleOnChange = this.handleOnChange.bind(this);
   this.onclick =  this.onclick.bind(this)
   this.refine = this.refine.bind(this)
   this.onPrioritySelect = this.onPrioritySelect.bind(this)
   this.setProximity = this.setProximity.bind(this)
  } 

  setProximity(id,score){
    let priority = this.state.priorityList
    for (let i of priority){
      if (i.id === id){
        i.score = score
        break
      }
    }
    this.setState({priorityList : priority})
  }


   onSelect(selectedList, selectedItem){
    this.setState({
      municipalityList: selectedList
    })
}

onPrioritySelect(selectedList, selectedItem){
  for (let i of selectedList){
    if (i.id === selectedItem.id){
      console.log('here')
      i.score = 0
    }
  }
  this.setState({
    priorityList: selectedList
  })
}

handleOnChange = (value) => {
  this.setState({minPrice: value[0],maxPrice:value[1]})
  }


   onclick(name){
     var dlist = this.state.structureList
     console.log(typeof(dlist),'123')
    if (name === 'apartment'){
      var flag = !this.state.apartmentFlag
      this.setState({apartmentFlag : !this.state.apartmentFlag})
      if (flag){
        dlist.push('apartment')
        this.setState({structureList: dlist})
      }else{
        dlist.pop('apartment')
        this.setState({structureList: dlist})
      }
    }else if (name === 'flat'){
      var flag = !this.state.flatFlag
      this.setState({flatFlag : !this.state.flatFlag})
      if (flag){
        dlist.push('flat')
        this.setState({structureList: dlist})
      }else{
        dlist.pop('flat')
        this.setState({structureList: dlist})
      }
    }else if (name === 'villa'){
      var flag = !this.state.villaFlag
      this.setState({villaFlag : !this.state.villaFlag})
      if (flag){
        dlist.push('villa')
        this.setState({structureList: dlist})
      }else{
        dlist.pop('villa')
        this.setState({structureList: dlist})
      }
    }else{
      var flag = !this.state.detachedFlag
      this.setState({detachedFlag : !this.state.detachedFlag})
      if (flag){
        dlist.push('detached')
        this.setState({structureList: dlist})
      }else{
        dlist.pop('detached')
        this.setState({structureList: dlist})
      }
    }
  }

  refine(){
    //console.log(this.state.structureList.length,this.state.municipalityList.length,this.state.minPrice,this.state.maxPrice)
    let finalList =  []
    let nlist = this.props.data
    
    if (this.state.municipalityList && this.state.municipalityList.length > 0){
    for (let n of nlist){
      for (let m of this.state.municipalityList){
        //console.log(n.municipality,m.id)
        if (n.municipality === m.id){
          finalList.push(n)
          break;
        }
      }
    }
  }else{
    finalList = this.props.data
  }
    nlist = JSON.parse(JSON.stringify(finalList));
    if (this.state.structureList.length != 0){  
    for (let n in nlist){
      let flag = true
      for (let struct of nlist[n].structureList){
        for (let s of this.state.structureList){
          if (s === struct){
            flag = false
            break
          }
        }
      }
      if (flag){
        finalList.pop(nlist[n])
      }
    }
  }
    nlist = JSON.parse(JSON.stringify(finalList));
    for (let n in nlist){
      console.log(nlist[n],nlist[n].minPrice,this.state.minPrice,nlist[n].maxPrice,this.state.maxPrice)
      if (nlist[n].minPrice >= this.state.minPrice && nlist[n].maxPrice <= this.state.maxPrice){
        console.log('working') 
      }else{
        finalList.pop(nlist[n])
      }
    }
    nlist = JSON.parse(JSON.stringify(finalList));
    this.setState({markerList: finalList})
  }

  render() {
     
    return (
      // Important! Always set the container height explicitly
      <div class='row' style={{ height: '120vh', width:'100%'}}>
      <div class='col-6' style={{padding:'0px'}}>
        <GoogleMapReact
          bootstrapURLKeys={{ key: 'AIzaSyC_HuHwk83Y_c1uBLIl2P_ZQ09gGtJsWlw' }}
          defaultCenter={this.props.center}
          defaultZoom={this.props.zoom}
        >
           {this.state.markerList.map((location) => (
                  <Marker
                  lat={location.lat}
                  lng={location.lng}
                  src="../assets/mapIcon.svg"
                />
      ))}

        
        </GoogleMapReact>
      </div>
      <div class='col-6' style={{backgroundColor: '#FBF6D9',padding:'0px'}}>
          <div style={{marginLeft:'30px',marginTop:'10px'}}>
            <h2>Refine your prefernces</h2>
          </div>
          <div style={{marginLeft:'40px',marginRight:'10px'}}>
            <div class='filter'>
               <div> 
               <h5>Choose a Municipality</h5>
               </div>

                <Multiselect
                    options={this.state.municipality} // Options to display in the dropdown
                    // selectedValues={this.state.selectedValue} // Preselected value to persist in dropdown
                    onSelect={this.onSelect} // Function will trigger on select event
                    onRemove={this.onSelect} // Function will trigger on remove event
                    displayValue="name" // Property name to display in the dropdown options
                    placeholder='Search'
                    className='multiselect'
                    showCheckbox='true'
                    style={this.style}
                                            />
            </div>
            <div class='filter'> 
                
                <div class='filter-head'><h5>Select price range</h5></div>
               <RangeSlider 
               min = {this.state.minPrice}
               max = {this.state.maxPrice}
               change =  {this.handleOnChange}
              
               />
            </div>
            <div class='filter'> 
               <div class='filter-head'> <h5>Select a structure type</h5> </div>
                <div class='row'>
                  <div class='col' onClick={() => this.onclick('apartment')}>
                <Structure
                class={this.state.apartmentFlag ? 'selected' : 'unselected'}
                name = 'Apartment'
                src = '/apartment.png'
                />
                </div>
                <div class='col' onClick={() => this.onclick('flat')}>
                <Structure
                class={this.state.flatFlag ? 'selected' : 'unselected'}
                name = 'Flat'
                src = '/flat.png'
                />
                </div>
                <div class='col' onClick={() => this.onclick('villa')}>
                <Structure
                class={this.state.villaFlag ? 'selected' : 'unselected'}
                name = 'Villa'
                src = '/villa.png'
                />
                </div>
                <div class='col' onClick={() => this.onclick('detached')}>
                <Structure
                class={this.state.detachedFlag ? 'selected' : 'unselected'}
                name = 'Detached'
                src = '/detached.png'
                />
                </div>
                </div>
            </div>
            <div class='filter'>
              <div class='filter-head'><h5>What's import for you?</h5></div>
              <div>
              <Multiselect
                    options={this.state.priorities} // Options to display in the dropdown
                    // selectedValues={this.state.selectedValue} // Preselected value to persist in dropdown
                    onSelect={this.onPrioritySelect} // Function will trigger on select event
                    onRemove={this.onPrioritySelect} // Function will trigger on remove event
                    displayValue="name" // Property name to display in the dropdown options
                    placeholder='Search'
                    className='multiselect'
                    showCheckbox='true'
                    style={this.style}
                               />
              </div>
            </div>
                {this.state.priorityList.map((priority) =>
                <div className='filter'>
                <PriorityCard
                name = {priority.name}
                id = {priority.id}
                change = {this.setProximity}
                score = {priority.score}
                />
                </div>
                )}
              
            <div class='button'>
              <button class='btn btn-success' onClick={this.refine}>Refine <ArrowForwardIcon/></button>
            </div>
          </div>
      </div>
      </div>
    );
  }
}